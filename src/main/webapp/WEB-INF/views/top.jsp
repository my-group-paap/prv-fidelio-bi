<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/includes.jsp" %>
<html>
<head>
  <title>Title here</title>
</head>

<body>
  <p1>Hey there! </p1>
  <p1>Current time: ${time} </p1>
  <table id="productList">
    <thead>
      <th>Name</th>
      <th>Quantity</th>
    </thead>
    <tbody>
<c:forEach items="${products}" var="product">
  <tr>
    <td>${product.name}</td>
    <td>${product.quantity}</td>
  </tr>

</c:forEach>
    </tbody>
  </table>
  </body>
</html>