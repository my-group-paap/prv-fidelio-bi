create table product
(
    id       BIGINT(20) NOT NULL AUTO_INCREMENT,
    name     VARCHAR(20),
    quantity INTEGER,
    PRIMARY KEY (id)
);
