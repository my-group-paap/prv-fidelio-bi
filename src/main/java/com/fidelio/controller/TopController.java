package com.fidelio.controller;


import com.fidelio.entities.Product;
import com.fidelio.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TopController {

    ProductService productService;

    @Autowired
    public TopController(ProductService productService){
        this.productService = productService;
    }

    @RequestMapping("/")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse resp) throws IOException {


        Map<String, Object> model = new HashMap<>();
        model.put("time", LocalDateTime.now());
        model.put("products", productService.getAllProducts());

        return new ModelAndView("top", model);
    }

}